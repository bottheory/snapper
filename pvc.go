package main

import (
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

var PVCGroupResource = schema.GroupVersionResource{Group: "", Version: "v1", Resource: "persistentvolumeclaims"}

func getPVCByName(ctx context.Context, kc dynamic.Interface, ns string, name string) (*v1.PersistentVolumeClaim, error) {
	found, err := kc.Resource(PVCGroupResource).Namespace(ns).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("error decoding pod object: %v", err)
	}

	var pvc v1.PersistentVolumeClaim
	err = runtime.DefaultUnstructuredConverter.
		FromUnstructured(found.Object, &pvc)
	if err != nil {
		return nil, fmt.Errorf("error decoding pod object: %v", err)
	}

	return &pvc, nil
}
