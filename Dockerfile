############################
# STEP 1 build executable binary
############################
FROM golang@sha256:6042b9cfb4eb303f3bdcbfeaba79b45130d170939318de85ac5b9508cb6f0f7e as builder

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.  Make is needed to build server.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git make ca-certificates && update-ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/home/appuser" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"
WORKDIR $GOPATH/src/mypackage/myapp/
COPY . .

# Build the binary
RUN make docker-dist

############################
# STEP 2 build a small image
############################
FROM scratch

# Import from builder
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy our static executable
COPY --from=builder /go/bin/snapper /go/bin/snapper

# Use an unprivileged user
USER appuser:appuser

# Run the golang binary
ENTRYPOINT ["/go/bin/snapper"]
