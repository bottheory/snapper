# Make does not offer a recursive wildcard function, so here's one:
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BINS := out/snapper

GOSRC := $(filter-out $(call rwildcard,.,*.pb.go),$(call rwildcard,.,*.go))

.PHONY: all prelease docker-dist test test-coverage
all: $(BINS)

$(BINS): $(GOSRC)

prerelease:
	go mod tidy
	go mod vendor

docker-dist: $(GOSRC)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags='-w -s -extldflags "-static"' -a \
      -o /go/bin/snapper .

test: $(BINS)
	SSE_CHROME="/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" \
	SSE_LOG_LEVEL=fatal \
		go test ./... -race -v -coverpkg=./...

out/snapper: $(GOSRC)
	mkdir -p out
	go build -v -i -o out snapper

