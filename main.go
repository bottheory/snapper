package main

import (
	"context"
	"fmt"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/dynamic"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"log"
	"os"
	"path/filepath"
	"time"
)

const (
	sourcePvc    = "sourcePvc"
	sourcePod    = "sourcePod"
	sourceLabels = "sourceLabels"
	snapName     = "snapName"
	suffix       = "suffix"
	class        = "storageClass"
	namespace    = "namespace"
	sleep        = "sleep"
	overwrite    = "overwrite"
)

func init() {
	pflag.String(sourcePvc, "", "name of the pvc you want to snapshot (use one of pvc, pod, or label)")
	pflag.String(sourcePod, "", "name of the pod whose pvc you want to snapshot (use one of pvc, pod, or label)")
	pflag.String(sourceLabels, "", "label of pod whose pvc you want to snapshot--will find the oldest creationDate (use one of pvc, pod, or label)")
	pflag.String(snapName, "", "name of the created snapshot (use either name or suffix)")
	pflag.String(suffix, "-snapshot", "suffix to add to name the created snapshot (use either name or suffix)")
	pflag.String(class, "", "the VolumeSnapshotClass to use for the snapshot")
	pflag.String(namespace, "default", "the namespace to use for the operation")
	pflag.String(overwrite, "true", "overwrite an existing snapshot if found")

	pflag.String(sleep, "0", "time to wait (s) after sending snapshot command before exit to allow for stabilization")

	viper.SetEnvPrefix("snapper")
	viper.SetConfigName("config")
	viper.AutomaticEnv()
}

func main() {
	pflag.Parse()
	err := viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		fmt.Printf("Error binding command line flags: %v\n", err)
		os.Exit(1)
	}

	ctx := context.Background()

	kubeClient, err := getKubeClient()
	if err != nil {
		fmt.Printf("Error getting k8s client: %v\n", err)
		os.Exit(1)
	}

	// Get the snapshot name from cmd line args
	var newSnapName = viper.GetString(snapName)
	if newSnapName == "" {
		fmt.Println("Must specify a snapName")
		os.Exit(1)
	}

	// Find the PVC names based on the cmd line args
	pvcName, err := getPvcFromArgs(ctx, kubeClient)
	if err != nil {
		fmt.Printf("Error getting PVC from arguments: %v\n", err)
		os.Exit(1)
	}

	fmt.Println("Creating snapshot from \"" + pvcName + "\"")

	if _, err := getPVCByName(ctx, kubeClient, viper.GetString(namespace), pvcName); err != nil {
		fmt.Printf("Error getting PVC with name \"%v\": %v\n", pvcName, err)
		os.Exit(1)
	}

	//
	// Delete any preexisting snapshot
	//
	if _, getErr := GetSnapshotByName(ctx, kubeClient, viper.GetString(namespace), newSnapName); getErr == nil {
		if viper.GetBool(overwrite) {
			fmt.Printf("Found existing shot, deleting it\n")
			if delErr := DelSnapshotByName(ctx, kubeClient, viper.GetString(namespace), newSnapName); delErr != nil {
				fmt.Printf("Error deleting existing snapshot: %v\nAborting.\n", delErr)
				os.Exit(1)
			}

			// Wait for deletion operation to complete
			gone := false
			for i := 0; i < 15; i++ {
				if _, getErr := GetSnapshotByName(ctx, kubeClient, viper.GetString(namespace), newSnapName); getErr != nil && errors.IsNotFound(getErr) {
					gone = true
					break
				}

				fmt.Println("Waiting for delete to complete...")
				time.Sleep(time.Second)
			}

			if !gone {
				fmt.Println("Deletion operation never completed, aborting.")
				os.Exit(1)
			}
		} else {
			fmt.Printf("Found existing shot but overwrite is false, exiting\n")
			os.Exit(0)
		}
	} else {
		if !errors.IsNotFound(getErr) {
			fmt.Printf("Unknown error checking for existing snapshot: %v\nAborting.\n", getErr)
			os.Exit(1)
		}
	}

	//
	// Create new snapshot
	//
	result, createErr := CreateSnapshotFromPVC(ctx, kubeClient, viper.GetString(namespace), pvcName, newSnapName, viper.GetString(class))
	if createErr != nil {
		log.Printf("Error creating snapshot: %v\n", createErr)
		os.Exit(1)
	} else {
		fmt.Printf("Created snapshot %q.\n", result.GetName())
	}

	time.Sleep(time.Duration(viper.GetInt(sleep)) * time.Second)
}

// getKubeClient first tries for an in-cluster config but will fall back to one in ~/.kube/config
func getKubeClient() (kc dynamic.Interface, err error) {
	// Setup Kube api connection, using the in-cluster config. This assumes the app is running in a pod.
	var config *rest.Config
	config, err = rest.InClusterConfig()
	if err != nil {
		if home := homedir.HomeDir(); home != "" {
			// Didn't work, so look for local file
			config, err = clientcmd.BuildConfigFromFlags("", filepath.Join(home, ".kube", "config"))
			if err != nil {
				return nil, fmt.Errorf("error getting k8s config: %v", err)
			}
		}
	}

	kc, err = dynamic.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("error getting k8s client: %v", err)
	}

	return kc, nil
}

func getPvcFromArgs(ctx context.Context, kubeClient dynamic.Interface) (pvcName string, err error) {
	pvcName = viper.GetString(sourcePvc)
	switch {
	case viper.GetString(sourcePvc) != "":

	case viper.GetString(sourcePod) != "":
		pvcName, err = GetPVCFromNamedPod(ctx, kubeClient, viper.GetString(namespace), viper.GetString(sourcePod))
		if err != nil {
			return "", fmt.Errorf("Error getting volume from pod: %v\n", err)
		}

	case viper.GetString(sourceLabels) != "":
		oldPod, err := GetOldestPodWithLabel(ctx, kubeClient, viper.GetString(namespace), viper.GetStringMapString(sourceLabels))
		if err != nil {
			return "", fmt.Errorf("Error finding pod by label: %v\n", err)
		}

		fmt.Printf("Snapshotting volume from pod with name: %v\n", oldPod)

		pvcName, err = GetPVCFromNamedPod(ctx, kubeClient, viper.GetString(namespace), oldPod)
		if err != nil {
			return "", fmt.Errorf("Error getting volume from pod: %v\n", err)
		}

	default:
		pflag.Usage()
		return "", fmt.Errorf("ERROR: must provide one of " + sourcePvc + " or " + sourcePod + " or " + sourceLabels)
	}

	return
}
