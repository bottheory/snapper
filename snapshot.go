package main

import (
	"bytes"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"text/template"
)

const snapTemplate = `{
"apiVersion":"snapshot.storage.k8s.io/v1beta1",
"kind":"VolumeSnapshot",
"metadata":{"name":"{{.SnapshotName}}"},
"spec":{"source":{"persistentVolumeClaimName":"{{.PvcSourceName}}"},"volumeSnapshotClassName":"{{.ClassName}}"}
}`

type snapshotParts struct {
	SnapshotName  string
	PvcSourceName string
	ClassName     string
}

var SnapshotGroupResource = schema.GroupVersionResource{Group: "snapshot.storage.k8s.io", Version: "v1beta1", Resource: "volumesnapshots"}

func GetSnapshotByName(ctx context.Context, kc dynamic.Interface, ns string, name string) (*unstructured.Unstructured, error) {
	return kc.Resource(SnapshotGroupResource).Namespace(ns).Get(ctx, name, metav1.GetOptions{})
}

func DelSnapshotByName(ctx context.Context, kc dynamic.Interface, ns string, name string) error {
	return kc.Resource(SnapshotGroupResource).Namespace(ns).Delete(ctx, name, metav1.DeleteOptions{})
}

func CreateSnapshotFromPVC(ctx context.Context, kc dynamic.Interface, ns string, source string, name string, class string) (*unstructured.Unstructured, error) {
	obj, err := newSnapshotObject(name, source, class)
	if err != nil {
		return nil, err
	}

	return kc.Resource(SnapshotGroupResource).Namespace(ns).Create(ctx, obj, metav1.CreateOptions{})
}

func newSnapshotObject(snapName string, pvcName string, class string) (*unstructured.Unstructured, error) {
	buf := new(bytes.Buffer)
	t := template.Must(template.New("snapshotManfiest").Parse(snapTemplate))
	err := t.Execute(buf, snapshotParts{
		SnapshotName:  snapName,
		PvcSourceName: pvcName,
		ClassName:     class,
	})
	if err != nil {
		return nil, err
	}

	obj := &unstructured.Unstructured{}
	_, _, err = unstructured.UnstructuredJSONScheme.Decode(buf.Bytes(), nil, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}
