package main

import (
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"time"
)

var PodGroupResource = schema.GroupVersionResource{Group: "", Version: "v1", Resource: "pods"}

func GetPVCFromNamedPod(ctx context.Context, client dynamic.Interface, ns string, podName string) (string, error) {
	// Get target pod
	found, getErr := client.Resource(PodGroupResource).Namespace(ns).Get(ctx, podName, metav1.GetOptions{})
	if getErr != nil {
		return "", fmt.Errorf("failed to get pod: %v", getErr)
	}

	var pod v1.Pod
	err := runtime.DefaultUnstructuredConverter.
		FromUnstructured(found.Object, &pod)
	if err != nil {
		return "", fmt.Errorf("error decoding pod object: %v", err)
	}

	foundPvc := ""
	for _, v := range pod.Spec.Volumes {
		if v.PersistentVolumeClaim == nil {
			continue
		}

		if foundPvc == "" {
			foundPvc = v.PersistentVolumeClaim.ClaimName
		} else {
			return "", fmt.Errorf("found more than one PVC on pod")
		}
	}

	if foundPvc == "" {
		return "", fmt.Errorf("no persistentVolumeClaim entry found in pod")
	}

	return foundPvc, nil
}

func GetOldestPodWithLabel(ctx context.Context, client dynamic.Interface, ns string, podLabels map[string]string) (string, error) {
	listOptions := metav1.ListOptions{
		LabelSelector: labels.SelectorFromSet(podLabels).String(),
	}

	pods, err := client.Resource(PodGroupResource).Namespace(ns).List(ctx, listOptions)
	if err != nil {
		return "", err
	}

	if len(pods.Items) == 0 {
		return "", fmt.Errorf("no pods found")
	}

	// Find oldest ready pod in results
	oldestPod := ""
	oldestDate := time.Now()
	for _, p := range pods.Items {
		var pod v1.Pod
		if err := runtime.DefaultUnstructuredConverter.
			FromUnstructured(p.Object, &pod); err != nil {
			return "", fmt.Errorf("error decoding pod object: %v", err)
		}

		if !isPodReady(pod) {
			continue
		}

		name := pod.Name
		ts := pod.CreationTimestamp

		if ts.Time.Before(oldestDate) {
			oldestPod = name
			oldestDate = ts.Time
		}
	}

	if oldestPod == "" {
		return "", fmt.Errorf("no ready pods found with labels %v", podLabels)
	}

	return oldestPod, err
}

func isPodReady(pod v1.Pod) bool {
	for _, condition := range pod.Status.Conditions {
		if condition.Type == v1.PodReady && condition.Status == v1.ConditionTrue {
			return true
		}
	}
	return false
}
